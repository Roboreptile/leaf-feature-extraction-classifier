from featuremaker import generate_features_labels
from categorizer import create_model,compile_model, train_model, display_analytics, save_model

# Load features and labels datasets into memory with selected features
features, labels = generate_features_labels(
    show_examples = False,
    feature_hu_moments = True,      #shape
    feature_zernike_moments = False, #shape
    feature_haralick = False,        #texture
    feature_lbp = False,             #texture
    feature_mean_variation = False,  #texture
    feature_histogram = False,       #color
    feature_mean_and_stddev = False, #color
)

# Creates the model with specified structure and properties
model = create_model(
    input_features_count = features.shape[1],
    neurons_per_layer = 128,
    deep_layers_count = 2, 
    dropout_rate = 0.1
)

# Compiles the model and prints its summary
model = compile_model(
    model = model,
    print_summary = True
)

# Trains the model over given amoount of epochs, keeps given percentage of data for validation
model, history, epochs = train_model(
    model = model,
    features = features,
    labels = labels,
    epochs = 200,
    validation_split = 0.2
)

# Displays the accuracy & loss graphs
display_analytics(
    history = history, 
    epochs = epochs
)

# Saves model into the folder with name specified below and unique id
save_model(
    model = model,
    file_name = "model"
)
