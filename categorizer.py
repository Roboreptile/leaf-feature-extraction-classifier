import tensorflow as tf
import numpy as np
from time import time
import matplotlib.pyplot as plt
from tensorflow.keras import layers

from const import *

def create_model(input_features_count, 
                neurons_per_layer = 512, 
                deep_layers_count = 1, 
                dropout_rate = 0.1):
    
    model = tf.keras.Sequential()

    # Input layer - batch normalization of input data
    model.add(layers.BatchNormalization(input_shape=[input_features_count]))

    # Deep layers - dense layer followed by batch norm and dropout.
    # Relu activation used as values of features are never < 0 + is faster to train
    for _ in range(deep_layers_count):
        model.add(layers.Dense(neurons_per_layer,activation='relu'))
        model.add(layers.BatchNormalization())
        model.add(layers.Dropout(dropout_rate))

    # Output layer - dense layer that will output an array with - hopefully - 0s everywhere except at the species' idx position
    model.add(layers.Dense(CATEGORIES_COUNT))
    
    return model


def compile_model(model:tf.keras.Sequential, print_summary = False):
    #Compiles the model with recommeded optimizer, selects accuracy as a measure statistic
    #Categorical crossentropy loss for comparison of the real label with the network output's one
    model.compile(
        optimizer='adam',
        loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
        metrics=['accuracy']
    )

    # Prints summary of the model, mostly for debug
    if print_summary: model.summary()

    return model

def train_model(model:tf.keras.Sequential, 
                features:np.array, 
                labels:np.array, 
                epochs, 
                validation_split,
                verbose = 1):
                
    if(verbose == 0): print("Training in progress...")
    # Train the model over 'epochs' epochs, keep 'validation_split' % data for validation
    history = model.fit(
        x = features,
        y = labels,
        validation_split=validation_split,
        epochs=epochs,
        verbose=verbose
    )

    return (model, history, epochs)

def display_analytics(history, epochs):

    # Displays graphs of accuracy and loss for a given set of features
    acc  = history.history['accuracy']
    loss = history.history['loss']
    val_acc  = history.history['val_accuracy']
    val_loss = history.history['val_loss']

    epochs_range = range(epochs)

    plt.figure(figsize=(8, 8))
    plt.subplot(1, 2, 1)
    plt.plot(epochs_range, acc, label='Training Accuracy')
    plt.plot(epochs_range, val_acc, label='Validation Accuracy')
    plt.legend(loc='upper left')
    plt.title('Accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, val_loss, label='Validation Loss')
    plt.legend(loc='upper left')
    plt.title('Loss')
    plt.show()
    
def save_analytics(history,epochs,feature_names):
    global saved_analytics

    # Generates graphs of accuracy and loss for a given set of features
    acc  = history.history['accuracy']
    loss = history.history['loss']
    val_acc  = history.history['val_accuracy']
    val_loss = history.history['val_loss']

    epochs_range = range(epochs)

    plt.figure(figsize=(8, 8))
    plt.suptitle(" + ".join(feature_names), size = 17)
    plt.subplot(1, 2, 1)
    plt.plot(epochs_range, acc, label='Training Accuracy')
    plt.plot(epochs_range, val_acc, label='Validation Accuracy')
    plt.legend(loc='upper left')
    plt.title('Accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, val_loss, label='Validation Loss')
    plt.legend(loc='upper left')
    plt.title('Loss')
    rounded_loss = round(val_acc[-1],3)
    feature_names_str = " ".join(feature_names)
    plt.savefig(GRAPHS_PATH + f'graph-acc {rounded_loss:.3f}-'+feature_names_str+'.png')

    # In addition, saves analytics to output them later on to the text file
    save_analytics_to_file([acc[-1],val_acc[-1],loss[-1],val_loss[-1],feature_names])
    # Returns average accuracy of the last 10 epochs
    return np.mean(val_acc[-10:-1],axis=0)

def save_analytics_to_file(analytics):

    # Saves analytics into the text file specified in const.py
    file = open(ANALYTICS_FILE,"a")
    #Removes the '[' ']' symbols from the line and writes it to the file
    file.write(str(analytics)[1:-1]+"\n")
    file.close()

def save_model(model:tf.keras.Sequential, file_name = "model"):
    
    # Create unique model name based on time
    name = MODELS_PATH + "{}-{}.h5".format(file_name,time())
    
    #Saves the model to the specified location
    model.save(name)
    print("Saved model as {}".format(name))

    
