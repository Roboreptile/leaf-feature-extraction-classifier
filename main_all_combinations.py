from const import DEBUG
from featuremaker import generate_buffer_of_all_features, get_selected_features_from_buffer, get_feature_names
from categorizer import create_model, compile_model, train_model, save_analytics
from itertools import product as all_possible_combinations

# This function is called multiple times in the main loop defined below
def test_for_features(buffer, buffer_len, feature_selection):

    # Get the features and labels from the buffer
    features, labels = get_selected_features_from_buffer(
        buffer = buffer, 
        buffer_len = buffer_len,
        feature_selection = feature_selection,
        multiply_dataset = 4
    )

    # Creates the model with specific properties
    model = create_model(
        input_features_count = features.shape[1],
        neurons_per_layer = 256,
        deep_layers_count = 3, 
        dropout_rate = 0.2
    )

    # Compiles the model
    model = compile_model(
        model = model,
        print_summary = False
    )
    
    # Trains the model over given amoount of epochs and validates it on validation data
    model, history, epochs = train_model(
        model = model,
        features = features,
        labels = labels,
        epochs = 200,
        validation_split = 0.2,
        verbose = 0
    )

    # Saves graphs of accuracy and loss, stores analytics to memory
    accuracy = save_analytics(
        history = history, 
        epochs = epochs,
        feature_names = get_feature_names(feature_selection)
    )

    # Returns average accuracy of the classifier
    return accuracy

if __name__ == "__main__":

    # Generates buffer of all possible features for all images, saves time - no need to open and process image multiple times!
    buffer, buffer_len = generate_buffer_of_all_features()

    # For every possible combination of a boolean array of size 7 starting at all 'True', ending at all 'False'
    for feature_selection in all_possible_combinations([True,False], repeat=7):
        # Do not process if no features are selected 
        if(
            feature_selection[0] == False and
            feature_selection[1] == False and
            feature_selection[2] == False and
            feature_selection[3] == False and
            feature_selection[4] == False and
            feature_selection[5] == False and
            feature_selection[6] == False
        ): continue
        
        # If accuracy is below 0.5, the function will repeat training up to 2 more times
        # sometimes the model fails to generalize to the data because of the dropout/incorrectly initialized weights
        accuracy = test_for_features(buffer, buffer_len, feature_selection)
        if(accuracy<0.5 and accuracy>0.1): accuracy = test_for_features(buffer, buffer_len, feature_selection)
        if(accuracy<0.5 and accuracy>0.1): accuracy = test_for_features(buffer, buffer_len, feature_selection)
        # In DEBUG mode, only one feature_selection is analyzed
        if DEBUG: break

    