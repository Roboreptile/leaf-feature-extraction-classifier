import numpy as np
import pandas
import cv2
import mahotas
import tensorflow as tf
import matplotlib.pyplot as plt

from const import *

def generate_features_labels(
        multiply_dataset = 4,           # Prevents the situation when during random selecting of the 
                                        # validation dataset, some categories don't appear in training / vice versa
        show_examples = False,          # shows some examples of images during loading phase, used for debug
        
        # Feature selection
        feature_hu_moments = True,      # shape feat.   - Computes Hu moments for the image
        feature_zernike_moments = True, # shape feat.   - Computes Zernike moments for the image
        feature_haralick = True,        # texture feat. - Computes Haralick features for the image
        feature_lbp = True,             # texture feat. - Computes Local Binary Patterns moments for the image
        feature_mean_variation = True,  # texture feat. - Computes Average Image Variation for the image
        feature_histogram = True,       # color feat.   - Computes Historgram for the image
        feature_mean_and_stddev = True, # color feat.   - Computes Mean and StdDev of colors for the image
    ):

    labels = []
    features = []
    
    #Loads paths from the data.tsv file provided in the dataset
    paths = get_paths()

    i = 0
    # Process every image - extract features and create feature vectors
    for img_path,seg_path,species in paths:

        # Create label [0... 1 ...0] with zeros for all entries except the one corresponding to the species idx
        label = np.zeros(CATEGORIES_COUNT)
        label[LEAF_CATEGORIES.index(species)] = 1

        # Load image, mask, resize them to the same size
        img = cv2.imread(img_path)
        img = cv2.resize(img, (IMAGE_SIZE,IMAGE_SIZE), interpolation=cv2.INTER_CUBIC)
        mask = cv2.imread(seg_path)
        mask = cv2.resize(mask, (img.shape[1],img.shape[0]), interpolation=cv2.INTER_LINEAR)
        
        # Show every 20th image as an example (for debug)
        if show_examples and i%20 == 0: debug_display(img,mask)
        
        #Apply the segmentation mask to the image, create grayscale and hsv images (used for feature generation)
        img = cv2.bitwise_and(img , mask)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        
        specific_features = []
        #Compute specific selected features
        if(feature_hu_moments): specific_features.append(extract_hu_moments(gray))
        if(feature_haralick): specific_features.append(extract_haralick(gray))
        if(feature_histogram): specific_features.append(extract_histogram(hsv))
        if(feature_lbp): specific_features.append(extract_local_binary_patterns(gray))
        if(feature_mean_and_stddev): specific_features.append(extract_mean_stddev(img))
        if(feature_mean_variation): specific_features.append(extract_mean_smoothness(img))
        if(feature_zernike_moments): specific_features.append(extract_zernike_moments(gray))
    
        #Create a numpy 1D feature vector from the specific features
        feature_vector = np.hstack(specific_features)
        
        # Append the data to the datasets
        features.append(feature_vector)
        labels.append(label)

        # This is just an animated icon, it looks nice
        if (i%8<=1):  c = '\\'
        elif(i%8<=3): c = '|'
        elif(i%8<=5): c = '/'
        else:         c = '-'
        
        i += 1
        print("\r{} Processing images... {}/{} {}".format(c,str(i),str(len(paths)),c),end="")
        
        # If DEBUG is turned on, stop the execution after loading only 100 images
        if DEBUG and i==100: break
        
    print("\nProcessing images finished!")

    # Repeats the datasets 'multiply_dataset' times
    features = features*multiply_dataset
    labels = labels* multiply_dataset
    
    # Normalizes the data by the maximum pixel color value
    features = np.array(features)/255
    labels = np.array(labels)

    #Randomly shuffles the data and returns the dataset
    return shuffle_features_labels(features,labels)

def generate_buffer_of_all_features():

    #Loads paths from the data.tsv file provided in the dataset
    paths = get_paths()
    
    hu = []
    zer = []
    hara = []
    lbp = []
    var = []
    hist = []
    meanstd = []
    
    labels = []
    
    i = 0
    # Process every image - extract features and create feature vectors
    for img_path,seg_path,species in paths:

        # Create categorical label
        label = np.zeros(CATEGORIES_COUNT)
        label[LEAF_CATEGORIES.index(species)] = 1

        # Loads and converts the image
        img = cv2.imread(img_path)
        img = cv2.resize(img[:-100,:-100], (IMAGE_SIZE,IMAGE_SIZE), interpolation=cv2.INTER_CUBIC)
        mask = cv2.imread(seg_path)
        mask = cv2.resize(mask, (img.shape[1],img.shape[0]), interpolation=cv2.INTER_LINEAR)

        img = cv2.bitwise_and(img , mask)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

        # Compute every feature and add it to memory
        hu.append(extract_hu_moments(gray))
        hara.append(extract_haralick(gray))
        hist.append(extract_histogram(hsv))
        lbp.append(extract_local_binary_patterns(gray))
        meanstd.append(extract_mean_stddev(img))
        var.append(extract_mean_smoothness(img))
        zer.append(extract_zernike_moments(gray))
        
        labels.append(label)

        # Animated loader
        if (i%8<=1):  c = '\\'
        elif(i%8<=3): c = '|'
        elif(i%8<=5): c = '/'
        else:         c = '-'
        i += 1

        print("\r{} Buffering images... {}/{} {}".format(c,str(i),str(len(paths)),c),end="")
        # DEBUG breaks after 10 images processed
        if DEBUG and i == 10: break
        
    print("\nBuffering images finished!")

    # Creates a dictionary buffer with key - name of the feature, and value - array with the computed features
    all_feature_names = get_feature_names([True]*7, add_labels = True)
    buffer = dict(zip(all_feature_names, [hu,zer,hara,lbp,var,hist,meanstd,labels]))
    buffer_len = i

    return buffer, buffer_len

def get_selected_features_from_buffer(buffer:dict, buffer_len, feature_selection, multiply_dataset = 4):
    # Get key-values for the dict from the input feature selection
    feature_names = get_feature_names(feature_selection)

    print("Extracting feature combination from buffer: "+" ".join(feature_names))
    #Prepare memory for the image batch
    features = [0]*buffer_len
    labels = [0]*buffer_len

    # Load selected features
    for i in range(buffer_len):
        vector = []
        for name in feature_names:
            vector.append(buffer[name][i])
            
        # Create a feature vector for every image
        features[i] = np.hstack(vector)
        labels[i] = buffer["Labels"][i]
    
    # Repeats dataset given amount of times
    features = features*multiply_dataset
    labels = labels* multiply_dataset

    #Normalizes the data
    features = np.array(features)/255
    labels = np.array(labels)

    #Shuffles randomly the batch
    return shuffle_features_labels(features, labels)

def get_paths():
    #Opens the data file
    tsv_file = open(DATA)
    #Reads it into memory
    tsv_data = pandas.read_csv(tsv_file, sep = '\t')

    #Extract the entries of interest - source: lab and species matching the ones selected in the const.py file
    filtered_data = tsv_data[
            tsv_data['source'].eq('lab') &
            tsv_data['species'].isin(LEAF_CATEGORIES)
    ]
    
    #Return the list with entries of format image_path, segmentation_mask_path, species_name
    return list(zip(
            filtered_data['image_path'], 
            filtered_data['segmented_path'],
            filtered_data['species']
    ))

### Functions below compute specific features, and are mostly self - explanatory
def extract_hu_moments(gray):
    feature = cv2.HuMoments(cv2.moments(gray))
    #print("Hu moments {}".format(feature))

    return feature.flatten()

def extract_haralick(gray):
    haralick = mahotas.features.haralick(gray)
    #print("Haralick {}".format(haralick))

    return haralick.mean(axis=0)

def extract_histogram(hsv):
    hist  = cv2.calcHist(
            [hsv],
            [0, 1, 2], 
            None, 
            [HIST_SIZE, HIST_SIZE, HIST_SIZE], 
            [0, 256, 0, 256, 0, 256]
    )
    cv2.normalize(hist, hist)
    #print("Hist {}".format(hist))

    return hist.flatten()

def extract_mean_stddev(img):
    mean, std = cv2.meanStdDev(img)
    mean_std = np.hstack([mean[0],mean[1],mean[2], std[0],std[1], std[2]])
    #print("Mean,Std {}".format(mean_std))

    return mean_std

def extract_local_binary_patterns(gray):
    patterns = mahotas.features.lbp(gray, radius = IMAGE_SIZE//10, points=HIST_SIZE)
    #print("Patterns {}".format(patterns))

    return patterns

def extract_zernike_moments(gray):
    moments = mahotas.features.zernike_moments(gray, radius = IMAGE_SIZE//10)
    #print("Zernike moments {}".format(moments))

    return moments

def extract_mean_smoothness(img):
    img_tensor = tf.convert_to_tensor(img, dtype=tf.int16)
    smoothness = tf.image.total_variation(img_tensor)
    mean_smoothness = smoothness/IMAGE_SIZE/IMAGE_SIZE/8
    #print("Smoothness {}".format(mean_smoothness))
    
    return [mean_smoothness]
#### End of feature functions

def debug_display(img,mask):
    # This function plots the base image, image mask, and image after mask is applied - debug purposes
    plt.figure(figsize=(6, 3))
    plt.subplot(1, 3, 1)
    plt.imshow(img)

    plt.subplot(1, 3, 2)
    plt.imshow(mask)

    plt.subplot(1, 3, 3)
    plt.imshow(cv2.bitwise_and(img,mask))

    plt.show()
    
def shuffle_features_labels(features,labels):
    #Randomly shuffle features and labels in the same way
    
    #Generate a random sequence of indexes without replacement
    randomize = np.arange(labels.shape[0])
    np.random.shuffle(randomize)
    
    #Swap order of the objects in the matrices
    features = features[randomize,:]
    labels = labels[randomize,:]

    return features, labels

def get_feature_names(features, add_labels = False):
    #Given input of a list of booleans, generate an array with names of features
    feature_names = []
    for idx,feature in enumerate(features):
        if feature==True:
            if   idx == 0: feature_names.append("Hu")
            elif idx == 1: feature_names.append("Zernike")
            elif idx == 2: feature_names.append("Haralick")
            elif idx == 3: feature_names.append("LBP")
            elif idx == 4: feature_names.append("Smooth")
            elif idx == 5: feature_names.append("Hist")
            elif idx == 6: feature_names.append("MeanStd")
    
    #While label is not a feature, it is used in a buffer as a key
    if add_labels: feature_names.append("Labels")
    return feature_names