# Leaf Feature Extraction & DNN Classifier 🍃

Contains graphs for every possible feature combination described in the solution

## Project Description:

Use feature extraction methods to create a classifier that takes these
features as an input and with decent accuracy categorizes leaves to their
corresponding species.

* [X] First implementation
* [X] Add more feature extractors
* [X] Discover the most optimal combination of features

## Before Running:

1. Download the following dataset:
   [LeafSnap Dataset](http://leafsnap.com/dataset/)
2. Extract *dataset*  folder to the solution folder
3. In the command line, navigate to the folder directory and run (preferably on a fresh python environment):
   Windows:
   `python -m pip install -r requirements.txt`
   Linux/Mac:
   `python3 -m pip install -r requirements.txt`

## Solution Description

There are two main scripts available.

- *main_selected_features.py* allows to test a specific combination of features. Setting the value of any to 'True' will include it inside the input vector. The comment (#) tells what kind of feature is extracted.

```
features, labels = generate_features_labels(
    feature_hu_moments = True,       #shape
    feature_zernike_moments = False, #shape
    feature_haralick = False,        #texture
    feature_lbp = False,             #texture
    feature_mean_variation = False,  #texture
    feature_histogram = False,       #color
    feature_mean_and_stddev = False, #color
)
```

After the training process finished, a graph is generated and saved to the *graphs* folder, and the model is saved to *models*  folder.

For all the basic features, after training for 200 epochs, this is the result accuracy:


| Feature | Accuracy | Verdict |
| - | - | - |
| Haralick | 0.82 | Good |
| Histogram | 0.03 | Bad |
| Hu Moments | 0.09 | Bad |
| LBP | 0.93 | Good |
| Smoothness | 0.20 | Bad |
| Zernike | 0.68 | Good |
| Mean & StdDev | 0.81 | Good |

*main_all_combinations.py* generates every possible combination of features mentioned above. When generating the graph, instead of reffering to the model ID, it now includes the string describing the features used.

Example:

![](demos/graph_example.png)

## Results

To test all features, after some tinkering I have found that the following model is the best:

![](demos/model.png)

After running the *main_all_combinations.py* script (around 6 hours) I created a table with all the results. Here is the best result + a short version of the full table (full version in *demos/Every possible Combination.pdf* )

Best feature vector:

![](demos/graph_best.png)

Short Table:

![](demos/demo.png)
